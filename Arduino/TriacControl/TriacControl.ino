#include <RFduinoBLE.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <PID_v1.h>

// I/O pins
#define TEMPERATURE             (6)
#define ZERO_CROSSING_DETECT    (5)
#define TRIAC_TRIGGER           (4)

// can rely on having this number of microseconds to work with each 1/2 AC wave
#define DELAY_MAXIMUM           (8900)
#define DELAY_MINIMUM           (1000)

// the time (microseconds) to delay turning on the triac in each 1/2 AC wave
volatile int triggerDelay;
// measured temperature from the DS18B20
double temperature;
// desired temperature from BLE controller
double setpoint;
// level of output from the PID library (set from 0.0 to 9900.0)
double output;

#define kP (700)
#define kI (1)
#define kD (1)

//Specify the links and initial tuning parameters
PID myPID(&temperature, &output, &setpoint, kP, kI, kD, DIRECT);

OneWire oneWire(TEMPERATURE);
DallasTemperature dsSensorControler(&oneWire);


// setup - enable comms, initialize counter, enable interrupts, set pin states
void setup(void) {

  configureTimer1();
  
  //turn the PID on
  myPID.SetOutputLimits(0.0, double(DELAY_MAXIMUM));
  myPID.SetMode(AUTOMATIC);
  
  RFduinoBLE.advertisementData = "TriacCon";
  RFduinoBLE.begin();

  triggerDelay = DELAY_MAXIMUM;
  temperature = 0.0;
  setpoint = 0.0;
  
  // triac trigger is an output
  pinMode(TRIAC_TRIGGER, OUTPUT);

  // negative pulse from opto/2N2222 triggers interrupt
  pinMode(ZERO_CROSSING_DETECT, INPUT);
  digitalWrite(ZERO_CROSSING_DETECT, HIGH);
  RFduino_pinWakeCallback(ZERO_CROSSING_DETECT, LOW, zeroCrossingInterrupt);
}

// loop - executed continually 
void loop(void) {

  // check temperature
  getCurrentCelsius(&temperature);

  // report it to the BLE controller
  RFduinoBLE.sendFloat(temperature);

  // PID library computes output based on current temperature
  myPID.Compute();

  setTriggerDelay();
}

void setTriggerDelay() {
  
  // output ranges from 0 to DELAY_MAXIMUM
  // delay reduces output. Max output is DELAY_MAXIMUM - DELAY_MAXIMUM = 0 delay
  // delay is clamped to min & max values before applying.
  
  double result = double(DELAY_MAXIMUM) - output;
  result = min((int)DELAY_MAXIMUM, (int)result);
  result = max((int)DELAY_MINIMUM, (int)result);
  
  // end result must be between max & min
  triggerDelay = (int)result;
}

void getCurrentCelsius(double *celsius) {
  dsSensorControler.begin();
  dsSensorControler.requestTemperatures();
  *celsius = double(dsSensorControler.getTempCByIndex(0));
}
  
void configureTimer1(void) {
  
  NRF_TIMER1->TASKS_STOP = 1;   // Stop timer
  NRF_TIMER1->MODE = TIMER_MODE_MODE_Timer;  // taken from Nordic dev zone
  NRF_TIMER1->BITMODE = (TIMER_BITMODE_BITMODE_16Bit << TIMER_BITMODE_BITMODE_Pos);
  NRF_TIMER1->PRESCALER = 4;   // SysClk/2^PRESCALER) =  16,000,000/16 = 1us resolution
  NRF_TIMER1->TASKS_CLEAR = 1; // Clear timer
  NRF_TIMER1->CC[0] = triggerDelay; // Cannot exceed 16bits
  NRF_TIMER1->INTENSET = TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos;  // taken from Nordic dev zone
  NRF_TIMER1->SHORTS = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);
  attachInterrupt(TIMER1_IRQn, TIMER1_Interrupt);    // also used in variant.cpp to configure the RTC1
}

void TIMER1_Interrupt(void) {

  if (NRF_TIMER1->EVENTS_COMPARE[0] != 0)
  {
    // switch the triac on
    digitalWrite(TRIAC_TRIGGER, HIGH);
  
    NRF_TIMER1->TASKS_STOP = 1; // stop timer
    NRF_TIMER1->CC[0] = triggerDelay;
  
    delayMicroseconds(10);  
    digitalWrite(TRIAC_TRIGGER, LOW);


    NRF_TIMER1->EVENTS_COMPARE[0] = 0;
  }
}

// Interrupt handler - fires once every zero crossing detect pulse
int zeroCrossingInterrupt(long unsigned int wtf) {

  if (triggerDelay < DELAY_MAXIMUM) {
    NRF_TIMER1->TASKS_START = 1;
  }
}

void RFduinoBLE_onReceive(char *data, int len){
  
  union {
    float value;
    unsigned char bytes[4];
  } received;
  
  for(int i = 0; i < 4; i++) {
    received.bytes[i] = *data++;
  }

  // BLE controller changes the setpoint
  setpoint = (double)received.value;
}

