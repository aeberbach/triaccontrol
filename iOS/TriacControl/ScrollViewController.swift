//
//  ScrollViewController.swift
//  TriacControl
//
//  Created by Adam Eberbach on 13/04/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//
//
//  Interfaces the RFDuino to the child view controllers that make up the scrolling display.
import UIKit

let DevelopingWithoutHardware : Bool = false

class ScrollViewController : UIViewController, UIScrollViewDelegate, RFduinoDelegate, RFDuinoSetpointChangeProtocol {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var scrollView: UIScrollView!

    var dataViewsAdded : Bool = false
    var dataViewControllers : [UIViewController]?
    let setpointControllerIndex = 0
    let historyControllerIndex = 1
    var rfduino : RFduino!

    // simulating hardware allows development to procced...
    var timer : NSTimer?
    var fakeTemperature : Float = temperatureMinimum
    var fakeTemperatureTrendUp : Bool = true
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        dataViewControllers = [
            storyboard.instantiateViewControllerWithIdentifier("SetpointViewController") as! SetpointViewController,
            storyboard.instantiateViewControllerWithIdentifier("HistoryViewController") as! HistoryViewController
        ]
        
        let setpointViewController = dataViewControllers![setpointControllerIndex] as! SetpointViewController
        setpointViewController.setpointDelegate = self
        
        if DevelopingWithoutHardware { // simulate some incoming temperature values
            timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "simulateData:", userInfo: nil, repeats: true)
        }
    }
    
    // only used to simulate temperature input when developing without hardware
    func simulateData(timer : NSTimer) {
        
        if fakeTemperatureTrendUp {
            fakeTemperature += 1.0
            if fakeTemperature > temperatureMaximum {
                fakeTemperature = temperatureMaximum
                fakeTemperatureTrendUp = false
            }
        } else {
            fakeTemperature -= 1.0
            if fakeTemperature < temperatureMinimum {
                fakeTemperature = temperatureMinimum
                fakeTemperatureTrendUp = true
            }
        }
        
        let fakeData = NSData(bytes: &fakeTemperature, length: sizeof(Float))
        self.didReceive(fakeData)
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        // only add the carousel views once
        if dataViewsAdded == true {
            return
        } else {
            dataViewsAdded = true
        }
        
        let scrollViewWidth = CGRectGetWidth(scrollView.bounds)
        let scrollViewHeight = CGRectGetHeight(scrollView.bounds)
        var xOffset : CGFloat = 0.0
        
        for viewController in dataViewControllers! {
            addChildViewController(viewController)
            viewController.view.frame = CGRectMake(xOffset, 0, scrollViewWidth, scrollViewHeight)
            scrollView.addSubview(viewController.view)
            xOffset += scrollViewWidth
        }
        scrollView.contentSize = CGSizeMake(xOffset, CGRectGetHeight(scrollView.frame))
    }

    @IBAction func segmentedControlAction(sender: UISegmentedControl) {
        let scrollWidth = CGRectGetWidth(scrollView.bounds)
        let offsetPoint = CGPointMake(CGFloat(sender.selectedSegmentIndex) * scrollWidth, 0)
        scrollView.setContentOffset(offsetPoint, animated: true)
    }
    
    //MARK:- RFduinoDelegate
    
    func didReceive(data: NSData!) {
        
        let receivedTemperature = dataFloat(data)
        
        println("received \(receivedTemperature)")
        // update temperature display in setpoint controller
        let setpointViewController = dataViewControllers![setpointControllerIndex] as! SetpointViewController
        setpointViewController.receivedTemp(receivedTemperature)
        let currentSetpoint = setpointViewController.setpoint
        
        // update history data and graph
        let historyViewController = dataViewControllers![historyControllerIndex] as! HistoryViewController
        historyViewController.addPoint(currentSetpoint, measuredTemp: receivedTemperature)
    }
    
    //MARK:- RFDuinoSetpointChangeProtocol
    
    func changeSetpoint(value : Float) {

        var mutableValue : Float = value
        var payload = NSData(bytes: &mutableValue, length: sizeof(Float))
        
        if DevelopingWithoutHardware == false {
            rfduino.send(payload)
        }
    }
}
