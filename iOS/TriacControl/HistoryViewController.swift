//
//  HistoryViewController.swift
//  TriacControl
//
//  Created by Adam Eberbach on 13/04/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//

import UIKit

class HistoryViewController : UIViewController {
    
    @IBOutlet weak var plottingScrollView: PlottingScrollView!

    let plotYGrowPoint : CGFloat = 40.0
    let rectDimension : CGFloat = 2.0
    let timestampsMargin = 60.0
    
    var currentPointYOffset : CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        plottingScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))
    }
    
    func addPoint(setpoint : Float, measuredTemp : Float) {

        plottingScrollView.addObservation(NSDate().timeIntervalSince1970, setpoint: setpoint, observedTemperature: measuredTemp)
    }
}

