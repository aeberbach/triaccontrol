//
//  AppDelegate.swift
//  TriacControl
//
//  Created by Adam Eberbach on 10/01/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var rfduinoManager : RFduinoManager?
    var wasScanning : Bool = false
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        rfduinoManager = RFduinoManager.sharedRFduinoManager()

        return true
    }

    func applicationWillResignActive(application: UIApplication) {

        wasScanning = false;
        
        if let manager = rfduinoManager {
            wasScanning = true
            manager.stopScan()
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {

        if wasScanning == true {
            
            if let manager = rfduinoManager {
                manager.startScan()
            }
            rfduinoManager?.startScan()
            wasScanning = false
        }
    }
}

