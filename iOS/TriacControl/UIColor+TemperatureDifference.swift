//
//  UIColor+TemperatureDifference.swift
//  TriacControl
//
//  Created by Adam Eberbach on 6/05/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func hotColorWithDegreesDifference(degrees : Float) -> UIColor {
        
        let colorSteps : Float = 30.0
        var differenceFactor:Float = min(1.0, degrees / colorSteps)

        return UIColor(red:1.0, green:CGFloat(1.0 - differenceFactor), blue:CGFloat(1.0 - differenceFactor), alpha:1.0)
    }

    class func coolColorWithDegreesDifference(degrees : Float) -> UIColor {

        let colorSteps : Float = 30.0
        var differenceFactor:Float = min(1.0, degrees / colorSteps)

        return UIColor(red:CGFloat(1.0 - differenceFactor), green:CGFloat(1.0 - differenceFactor), blue:1.0, alpha:1.0)
    }
}