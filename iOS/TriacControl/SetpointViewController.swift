//
//  SetpointViewController.swift
//  TriacControl
//
//  Created by Adam Eberbach on 11/01/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//
//  handle sending setpoint to hardware controller when it changes, reporting of current temp and status

import UIKit

let temperatureMinimum : Float = -55.0 // minimum for Dallas 18B20
let temperatureMaximum : Float = 125.0 // maximum for Dallas 18B20
let setpointDefault : Float = 25.0 // if no stored value

protocol RFDuinoSetpointChangeProtocol {

    func changeSetpoint(value : Float)
}

class SetpointViewController: UIViewController {

    let setpointKey = "TriacControlTemperatureSetpoint"
    var setpoint : Float = setpointDefault
    var temperature : Float?
    var setpointDelegate : RFDuinoSetpointChangeProtocol?
    var rfduino : RFduino!
    
    @IBOutlet weak var setpointLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var setpointSlider: UISlider!
    
    override func viewDidLoad() {

        // slider configuration
        setpointSlider.minimumValue = temperatureMinimum
        setpointSlider.maximumValue = temperatureMaximum

        // attempt to find previous temperature setpoint
        let userDefaults = NSUserDefaults.standardUserDefaults()
        var storedValue = userDefaults.floatForKey(setpointKey)
        
        if storedValue == 0.0 {
            setpoint = setpointDefault
        } else {
            setpoint = storedValue
        }
        setpointSlider.value = setpoint
        labelSetpoint(setpoint)
        // don't know the temp until BT updates
        receivedTemp(FLT_MAX)
    }
    
    // MARK:- Handle setpoint slider changes

    @IBAction func setpointIncrementAction(sender: AnyObject) {
        
        setpointSlider.value += 0.1
        setpoint = setpointSlider.value
        setpointUpdate()
    }
    
    @IBAction func setpointDecrementAction(sender: AnyObject) {

        setpointSlider.value -= 0.1
        setpoint = setpointSlider.value
        setpointUpdate()
    }
    
    @IBAction func setpointChanged(sender: UISlider) {

        setpoint = sender.value
        setpointUpdate()
    }

    func setpointUpdate() {
        
        // create and send payload NSData to RFduino
        setpointDelegate?.changeSetpoint(setpoint)
        
        // update label value
        labelSetpoint(setpoint)
        
        // write value to user defaults
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setFloat(setpoint, forKey: setpointKey)
    }
    
    // MARK:- convenience set labels

    func labelSetpoint(value : Float) {
        
        setpointLabel.text = NSString(format: "%.1f", value) as String
    }

    func receivedTemp(value : Float) {
        
        temperature = value
        if value > temperatureMaximum || value < temperatureMinimum {
            temperatureLabel.text = NSString(string: "?") as String
        } else {
            temperatureLabel.text = NSString(format: "%.1f", value) as String
        }
        updateDisplayColors()

    }
    
    //MARK:- Temperature display colors
    
    // Values are displayed using a blue/red scheme indicating cold/hot. The further the temperatures are apart the more
    // saturated the color.
    func updateDisplayColors() {
        
        var hotterLabel : UILabel
        var coolerLabel : UILabel
        var degrees : Float
        
        // find which is hotter, setpoint or temperature
        if temperature > setpoint {
            hotterLabel = temperatureLabel
            coolerLabel = setpointLabel
            degrees = temperature! - setpoint
        } else {
            hotterLabel = setpointLabel
            coolerLabel = temperatureLabel
            degrees = setpoint - temperature!
        }
        
        // color is set by subtracting equal amounts from the non-red or non-blue channels
        hotterLabel.textColor = UIColor.hotColorWithDegreesDifference(degrees)
        coolerLabel.textColor = UIColor.coolColorWithDegreesDifference(degrees)
    }
}