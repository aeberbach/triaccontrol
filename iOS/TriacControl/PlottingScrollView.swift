//
//  PlottingScrollView.swift
//  TriacControl
//
//  Created by Adam Eberbach on 13/04/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//

import UIKit

let plotTemperatureMinimum : Float = 15.0
let plotTemperatureMaximum : Float = 100.0

class PlottingScrollView : UIScrollView {
    
    // margin to keep the lines from hitting view edge
    let leftMarginWidth : CGFloat = 20.0
    let rightMarginWidth : CGFloat = 20.0
    
    // length of line to add per observation
    let verticalAdvanceAmount : CGFloat = 0.25
    let verticalScrollAmount : CGFloat = 30.0
    
    // record setpoints, temperature, timestamps, labels
    var setpoints : [Float] = []
    var temperatures : [Float] = []
    
    // paths and positions for drawing
    var temperaturePath : CGMutablePathRef?
    var setpointPath : CGMutablePathRef?
    var yOffset : CGFloat = 0.0
    
    // add a new observation to the temperature paths
    func addObservation(timestamp : NSTimeInterval, setpoint : Float, observedTemperature : Float) {

        // constrain
        var constrainedTemperature = max(plotTemperatureMinimum, observedTemperature)
        constrainedTemperature = min(plotTemperatureMaximum, constrainedTemperature)
        
        var constrainedSetpoint = max(plotTemperatureMinimum, setpoint)
        constrainedSetpoint = min(plotTemperatureMaximum, setpoint)

        // when the number of points to be displayed multiplied by the vertical advance would exceed this view's height, remove some of
        // the oldest points before appending the new point. This gives the effect of a scrolling graph.
        
        let graphHeight = (CGFloat(setpoints.count) + 1.0) * verticalAdvanceAmount
        if graphHeight > CGRectGetHeight(self.bounds) {
            let range = Range(start: 0, end: 10)
            setpoints.removeRange(range)
            temperatures.removeRange(range)
        }
        
        setpoints.append(constrainedSetpoint)
        temperatures.append(constrainedTemperature)
        
        // after updating the graph cause it to be drawn
        createPathsForTemperatures()
        setNeedsDisplay()
    }
    
    // calculate the x offset for the temperature point
    func xOffsetForTemperature(temperature : Float) -> CGFloat {
        
        let temperatureRange = fabsf(plotTemperatureMaximum) + fabsf(plotTemperatureMinimum)
        
        let displayWidth = CGRectGetWidth(bounds) - leftMarginWidth - rightMarginWidth
        let pixelsPerDegree = Float(displayWidth) / temperatureRange
        var plotOffset : Float = 0.0;
        if plotTemperatureMinimum < 0 {
            plotOffset = fabsf(plotTemperatureMinimum)
        }
        let shiftedTemperature = temperature + plotOffset
        return leftMarginWidth + CGFloat(shiftedTemperature * pixelsPerDegree)
    }

    func createPathsForTemperatures() {
        
        var yOffset : CGFloat = 0.0

        setpointPath = CGPathCreateMutable()
        CGPathMoveToPoint(setpointPath, nil, xOffsetForTemperature(setpoints[0]), yOffset)
        var remainingSetpoints = setpoints
        remainingSetpoints.removeAtIndex(0)
        
        temperaturePath = CGPathCreateMutable()
        CGPathMoveToPoint(temperaturePath, nil, xOffsetForTemperature(temperatures[0]), yOffset)
        var remainingTemperatures = temperatures
        remainingTemperatures.removeAtIndex(0)

        for value in remainingSetpoints {
            
            yOffset += verticalAdvanceAmount
            let setpointXOffset = xOffsetForTemperature(value)
            CGPathAddLineToPoint(setpointPath, nil, setpointXOffset, yOffset)
        }

        yOffset = 0.0
        for value in remainingTemperatures {
            
            yOffset += verticalAdvanceAmount
            let temperatureXOffset = xOffsetForTemperature(value)
            CGPathAddLineToPoint(temperaturePath, nil, temperatureXOffset, yOffset)
        }
    }
    

    override func drawRect(rect: CGRect) {
      
        super.drawRect(rect)
        
        let context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, 2.0)
        
        // set setpoint color, stroke
        if let setpointPath = setpointPath {
            CGContextSetStrokeColorWithColor(context, UIColor.greenColor().CGColor)
            CGContextAddPath(context, setpointPath)
            CGContextStrokePath(context)
        }

        // set temp color, stroke
        if let temperaturePath = temperaturePath {
            CGContextSetStrokeColorWithColor(context, UIColor.redColor().CGColor)
            CGContextAddPath(context, temperaturePath)
            CGContextStrokePath(context)
        }
    }
}