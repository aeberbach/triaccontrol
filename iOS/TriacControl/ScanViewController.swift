//
//  ScanViewController.swift
//  TriacControl
//
//  Created by Adam Eberbach on 10/01/2015.
//  Copyright (c) 2015 Adam Eberbach. All rights reserved.
//
// On startup, search for a compatible controller. Proceed to control view controller if found

import UIKit

class ScanViewController: UIViewController, RFduinoManagerDelegate {

    let expectedAdvertisementName = "TriacCon"
    let scrollViewSegueIdentifier = "ScrollViewControllerSegue"
    
    let rfduinoManager = RFduinoManager.sharedRFduinoManager()

    var discoveredRFduino : RFduino!
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == scrollViewSegueIdentifier {

            var controlViewController = segue.destinationViewController as! ScrollViewController
            controlViewController.rfduino = discoveredRFduino
            controlViewController.rfduino.delegate = controlViewController
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // look for a matching arduino. Might need to add a list of controllers one
        // day in case a user has multiples but for now take the first with the right ID
        rfduinoManager.delegate = self;
    }

    func isMatchingRFduino(rfduino : RFduino) -> Bool {
        
        if rfduino.advertisementData != nil {
            var advertising = NSString(data: rfduino.advertisementData, encoding: NSUTF8StringEncoding)
            
            if let foundString = advertising {
                println("\(foundString)")
                if foundString.isEqualToString(expectedAdvertisementName) {
                    return true
                }
            }
        }
        return false
    }
    
    //MARK:- RFduinoManagerDelegate

    func didDiscoverRFduino(rfduino: RFduino!) {
    
        if isMatchingRFduino(rfduino) == true {
            println("It's a match")
            rfduinoManager.connectRFduino(rfduino)
        } else {
            println("Instead, found \(NSString(data: rfduino.advertisementData!, encoding: NSUTF8StringEncoding))")
        }
        
    
    }
    
    func didConnectRFduino(rfduino: RFduino!) {
        
        // connection established, so stop scanning
        rfduinoManager.stopScan()
        // segue to the control view controller
        discoveredRFduino = rfduino
        performSegueWithIdentifier(scrollViewSegueIdentifier, sender: self)
    }
    
    func didDisconnectRFduino(rfduino: RFduino!) {

        println("Arduino disconnected!")
    }
}

