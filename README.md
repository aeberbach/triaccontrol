# Triac Control #

Control an AC resistive load (such as a water heater) with a triac. A simple circuit detects AC zero-crossing and uses this to trigger a bluetooth-enabled device programmed through the Arduino IDE (RFDuino) to repeatedly trigger a triac, providing variable power output. A PID library is used to regulate the output level based on the measured temperature and the setpoint.

The iOS app used to read the temperature and to change the setpoint is simple and only reads and writes values from the BLE controller. In the stepping view, values are displayed in a bluish tint for the cooler value and a reddish tint for the warmer value, moving to white as the numbers converge. In the history display a simple plot of temperature vs. time shows the convergence of temperature (red) and setpoint (green). 

see: Make magazine vol. 25